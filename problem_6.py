# Please use python version 3.6
# Yogesh Kushwaha 20180405

# find maximum 2 and minimum 2 elements of a list
# puts them into MinList and MaxList
# then find the average of MinList and MaxList

def maxtwo(list):
    list = sorted(list, reverse = True)
    list = list[:2]
    return(list)

def mintwo(list):
    list = sorted(list)
    list = list[:2]
    return(list)

def avg(list):
    return(sum(list)/len(list))

list1 = [23, 21, 12, 1, 43, 76]
list2 = [32, 24, 73, 93, 3, 55]
list3 = [2, 8, 86, 33, 82, 11]

print("List 1: ", list1)
print("List 2: ", list2)
print("List 3: ", list3)

MaxList = []
MaxList = maxtwo(list1) + maxtwo(list2) + maxtwo(list3)
print("MaxList: ", MaxList)
print("Average: ", avg(MaxList))

MinList = []
MinList = mintwo(list1) + mintwo(list2) + mintwo(list3)
print("Mislist: ", MinList)
print("Average: ", avg(MinList))