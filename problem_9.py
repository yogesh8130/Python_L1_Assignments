# python 3.6 used
# yogesh kushwaha

# find empty files

class File:
    fileCount = 0
    def __init__(self, date, time, size, name):
        self.date = date
        self.time = time
        self.size = size
        self.name = name
        File.fileCount += 1
    def totalFiles(self):
        print("Total number of files are: %d" % fileCount)
    def displayFiles(self):
        print(self.date, self.time, self.size, self.name)
    def isEmpty(self):
        if self.size == 0:
            return True
        else:
            return False

fileList = []
# populating the fileList
fileList.append(File("02/15/2016", "10:49 PM", 962, "switchfinal.py"))
fileList.append(File("02/15/2016", "10:49 PM", 943, "switchfinal.py.bak"))
fileList.append(File("01/27/2016", "11:46 AM", 15, "t.py"))
fileList.append(File("03/31/2016", "12:39 PM", 840, "t1.py"))
fileList.append(File("01/25/2016", "10:34 AM", 2407, "tc1.py"))
fileList.append(File("02/14/2017", "09:13 AM", 0, "teat.py"))
fileList.append(File("03/15/2016", "05:52 PM", 5, "tes.py"))

print("===============================================")
print("complete filelist:")
print("-----------------------------------------------")
for f in fileList:
    f.displayFiles()

print("===============================================")
print("Following are the empty files (size = 0)")
print("-----------------------------------------------")
for f in fileList:
    if f.isEmpty():
        f.displayFiles()