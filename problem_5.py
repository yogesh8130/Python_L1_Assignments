# using python 3.6
# YOGESH KUSHWAHA

# counts the characters and strings in a text file and produces results in output.txt file

try:
    filein = open("input.txt","r")
except:
    print("input.txt should exist in the same folder")
    print("I don't think it exists")

fileout = open("output.txt","w")

fileout.write("Counting whitespace as character because they technically are.")
fileout.write("\n(also because I wasn't told not to)")
fileout.write("\n\nString No.\tChar Count\tWord Count")

print("Doing Stuff")
nstring = 1
string = filein.readline()
while(string):
    nchar = len(string)
    nword = len(string.split())       

    fileout.write("\nstring %d\t\t%d\t\t\t%d" % (nstring, nchar, nword))
    
    string = filein.readline()
    nstring += 1

filein.close()
fileout.close()
print("Done stuff")
print("Please check the file output.txt")
