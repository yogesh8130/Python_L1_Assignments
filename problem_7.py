# using python 3.6
# YOGESH KUSHWAHA

# convert network prefix to IP address

while True:
    prefix = int(input("Enter the network prefix (1 - 32): "))
    if prefix < 1 or prefix > 32:
        print("Invalid value")
        continue
    else:
        break

mask = ""
ip = []

for i in range (0, 4):
    for i in range (0, 8):
        if (prefix > 0):
            mask = mask + "1"
        else:
            mask = mask + "0"
        prefix -= 1
    ip.append(str(int(mask, 2)))
    mask = ""

print(":".join(ip))

