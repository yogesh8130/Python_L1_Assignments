# python 3.6 used
# yogesh kushwaha

# xml parser

import xml.etree.ElementTree as ET

class Book:
    bookCount = 0
    def __init__(self, title, author, year, price, category):
        self.title = title
        self.author = author
        self.year = year
        self.price = price
        self.category = category
        Book.bookCount += 1
    def totalBooks(self):
        print("Total number of books are: %d" % Book.bookCount)
    def displayBook(self):
        print("Title\t: ", self.title)
        print("Author\t: ", self.author)
        print("Year\t: ", self.year)
        print("Price\t: ", self.price)
        print("Category: ", self.category)

tree = ET.parse("books.xml")
root = tree.getroot()

# booklist will contain all book objects, it is our catalog
booklist = []
for book in root.findall("book"):
    title = book.find("title").text
    author = book.find("author").text
    year = book.find("year").text
    price = book.find("price").text
    category = book.attrib["category"]
    # adding the found book to the booklist
    booklist.append(Book(title, author, year, price, category))

print("The following books were added to the catalog:")
print("==============================================")

for i in booklist:
    i.displayBook()
    print("---------------------------")

Book.totalBooks(booklist)