# using python 3.6
# YOGESH KUSHWAHA

# program to check for two e's

def twoE(mystring):
    ecount = 0
    for i in mystring:
        if (i=='e' or i=='E'):
            ecount = ecount + 1
    if (ecount == 2):
        print ("\nYes There were exactly 2 e's, so I'm returning True")
        print ("You win!! I guess...\n")
        return True
    else:
        print ("\noops there were %d e's which is clearly not two, so I'm returning False" % (ecount))
        print ("You must try again...\n")
        return False

while True:
    mystring = input("Enter a string of your choice (Be a bit creative): ")
    twoE(mystring)
