# using python 3.6
# YOGESH KUSHWAHA (This file is free to use, if it's any use (CC BY-SA 3.0 license) ;)
# On directly copy pasting the code gives lot of errors
# so I guess I'll make some edits, that's the point of assignment right?
# I'll mention what I'm changing and why.

mylist = list(range(4))     # If I just write mylist = range(4) then instead of making a list,
                            # "mylist" becomes a range object and append statement throws an error.
seclist = mylist

# I'm changing the print statement because I'm using python 3.6; and the following is better than old one.
print(seclist)              # output: [0, 1, 2, 3]

mylist.append(4)
print(seclist)              # output: [0, 1, 2, 3, 4]
                            # but I appended to mylist... I get what's happening here.
                            # because both seclist and mylist are using same memory space.

seclist = mylist[:]         # now this is different because all elements are now copied to seclist
                            # now updating one list wont change another

print(seclist)              # output: [0, 1, 2, 3, 4]

mylist.append(5)
print(seclist)              # output: [0, 1, 2, 3, 4]
                            # see adding 5 to original list doesn't do anything to another list
                            # they live in seperate houses now.

# many thanks to the question framer
# I really didn't know this before, I learned something new today
# 20180404
