# using python 3.6
# YOGESH KUSHWAHA (This file is free to use, if it's any use (CC BY-SA 3.0 license) ;)

def f(n):
    for x in range(n):
        yield x**3          # yield will make a 'generator', returns a value at every iteration as soon as 'yield' is reached
                            # passes values to the caller one by one as they are calculated
                            # return doesn't do generators and instead returns only after the loop has run completely
                            # can save memory when used correctly
for x in f(6):
    print(x)

## output:
## 0
## 1
## 8
## 27
## 64
## 125
